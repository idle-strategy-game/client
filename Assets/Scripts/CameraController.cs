﻿using UnityEngine;

public class CameraController : MonoBehaviour {
  private Vector3 dragOrigin;
  public float dragSpeed = -1f;
  public int minSize = 20;
  public int maxSize = 100;

  void Update() {
    if (Input.GetMouseButtonDown(0)) {
      dragOrigin = Input.mousePosition;
      return;
    }

    if (!Input.GetMouseButton(0)) {
      return;
    }

    Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - dragOrigin);
    Vector3 move = new Vector3(Camera.main.orthographicSize * dragSpeed * pos.x, Camera.main.orthographicSize * dragSpeed * pos.y, 0);
    transform.Translate(move, Space.World);
    dragOrigin = Input.mousePosition;
  }

  void OnGUI() {
    Camera.main.fieldOfView -= Input.mouseScrollDelta.y;
    if (Camera.main.fieldOfView < minSize) {
      Camera.main.fieldOfView = minSize;
    } else if (Camera.main.fieldOfView > maxSize) {
      Camera.main.fieldOfView = maxSize;
    }
  }
}
