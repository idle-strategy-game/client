﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellController : MonoBehaviour {
  public Cell cellData;
  public int voronoiId;
  public List<Vector2> vertices2D = new List<Vector2>();
  public Vector3[] vertices3D;
  public List<GameObject> neighboors = new List<GameObject>();
  public string type;
  public Material mat;
  
  public bool isSelected = false;
  public GameObject uiCellPanel;

  // Start is called before the first frame update
  void Start() {
    createMeshAndUVs();
    this.type = cellData.type;  
    this.voronoiId = cellData.voronoiId;

    switch (type) {
      case "water":
        mat = Resources.Load("Materials/WaterMaterial") as Material;
        break;

      case "grass":
        mat = Resources.Load("Materials/GrassMaterial") as Material;
        break;

      case "forest":
        mat = Resources.Load("Materials/ForestMaterial") as Material;
        break;

      case "stonePit":
        mat = Resources.Load("Materials/RockMaterial") as Material;
        break;

      case "goldMine":
        mat = Resources.Load("Materials/GoldMaterial") as Material;
        break;

      default: 
        mat = Resources.Load("Materials/GrassMaterial") as Material;
        break;
    }
    GetComponent<MeshRenderer>().material = mat;
  }

  void OnMouseUpAsButton() {
    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
    RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);

    if(hit && hit.collider != null) {
      if (hit.collider.gameObject == gameObject) {
        if (type != "water") {
          GameObject uiCellPanel = GameObject.Find("UI").transform.Find("Cell").gameObject;
          uiCellPanel.GetComponent<UiController>().updateSelectedCell(gameObject);
        }
      }
    }
  }

  // Update is called once per frame
  void Update() {

  }

  public void toggleSelected() {
    this.isSelected = !this.isSelected;

    if (!this.isSelected) {
      gameObject.GetComponent<LineRenderer>().widthMultiplier = 1f;
      gameObject.GetComponent<LineRenderer>().material = Resources.Load("Materials/Black") as Material;
    } else {
      gameObject.GetComponent<LineRenderer>().widthMultiplier = 5f;
      gameObject.GetComponent<LineRenderer>().material = Resources.Load("Materials/White") as Material;
    }
  }

  void createMeshAndUVs() {
    // Retrieve clockwise points
    foreach (Point p in cellData.pointsSorted) {
      vertices2D.Add(new Vector2(p.x, p.y));
    }

    // Triangulate the polygon
    Triangulator tri = new Triangulator(vertices2D.ToArray());
    int[] indices = tri.Triangulate();
    
    // Create the Vector3 vertices (z = 0)
    this.vertices3D = new Vector3[vertices2D.ToArray().Length];
    for (int i = 0; i < this.vertices3D.Length; i++) {
      this.vertices3D[i] = new Vector3(vertices2D[i].x, vertices2D[i].y, 0);
    }

    // Create the mesh
    Mesh msh = new Mesh();
    msh.vertices = vertices3D;
    msh.triangles = indices;
    msh.RecalculateNormals();
    msh.RecalculateBounds();

    // Create the UVs
    Vector2[] myUVs = new Vector2[vertices3D.Length]; // Create array with the same element count
    for (int i = 0; i < vertices3D.Length; i++) {
      myUVs[i] = new Vector2(vertices3D[i].x, vertices3D[i].y);
    } // Useless ?
    msh.uv = myUVs;

    // Define the MeshRenderer 
    gameObject.AddComponent(typeof(MeshRenderer));
    MeshFilter filter = gameObject.AddComponent(typeof(MeshFilter)) as MeshFilter;
    filter.mesh = msh;

    Vector3[] verticesUV = msh.vertices;
  }

  void createCollider() {
    gameObject.AddComponent(typeof(PolygonCollider2D));
    gameObject.GetComponent<PolygonCollider2D>().points = vertices2D.ToArray();
  }

  void createOutline() {
    gameObject.AddComponent(typeof(LineRenderer));
    LineRenderer lr = gameObject.GetComponent<LineRenderer>();
    lr.positionCount = vertices3D.Length;
    lr.SetPositions(vertices3D);
    lr.loop = true;
    if (type == "water") { 
      lr.widthMultiplier = 0f;
    } else {
      lr.widthMultiplier = 1f;
    }
    lr.material = Resources.Load("Materials/Black") as Material;
    lr.sortingOrder = 1;
  }
}
