﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Networking;

public class CellsController : MonoBehaviour {
  private string serverUrl = "http://localhost:8081";
  private Vector3 offset;
  public CellCollection cellCollection;
  public List<GameObject> cells = new List<GameObject>();
  public Camera mainCamera;

  // Start is called before the first frame update
  void Start() {
    fetchCells();
  }

  // Update is called once per frame
  void Update() {
    
  }

  public void fetchCells() {
    StartCoroutine(FetchCellsRequest());
  }

  IEnumerator FetchCellsRequest() {
    using (UnityWebRequest uwr = UnityWebRequest.Get(serverUrl + "/worldData")) {
      // Request and wait for the desired page.
      yield return uwr.SendWebRequest();

      if (uwr.isNetworkError) {
        Debug.Log("Error while fetching resources: " + uwr.error);
      } else {
        generateCells(uwr.downloadHandler.text);
      }
    }
  }

  void generateCells(string worldData) {
    cellCollection = JsonUtility.FromJson<CellCollection>(worldData); // TODO move cell related informations in children instead of holding it here

    foreach (Cell cell in cellCollection.cells) {
      // Create the GameObject
      var go = new GameObject();
      go.transform.parent = GameObject.Find("World/Cells").transform;
      go.name = "Cell" + cell.voronoiId;
      go.tag = "Cell";
      go.AddComponent(typeof(CellController));
      go.GetComponent<CellController>().cellData = cell;
      this.cells.Add(go);
    }

    affectAllNeighboors(); // Only after all cells have been created !
  }

  void affectAllNeighboors() {
    foreach (GameObject cell in this.cells) {
      CellController cellCtl = cell.GetComponent<CellController>();

      for (int i = 0; i < cellCtl.cellData.neighboors.Length; i++) {
        cellCtl.neighboors.Add(GameObject.Find("World/Cells/Cell" + cellCtl.cellData.neighboors[i].ToString()));
      }
    }
  }
}
