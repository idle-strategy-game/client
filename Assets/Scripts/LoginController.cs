﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class LoginController : MonoBehaviour {
  private string serverUrl = "http://localhost:8081";
   
  [System.Serializable]
  public class Registration {
    public string username;
    public string password;
    public string email;

    public Registration(string username, string password, string email) {
      this.username = username;
      this.password = password;
      this.email = email;
    }
  }

  [System.Serializable]
  public class Authentication {
    public string username;
    public string password;

    public Authentication(string username, string password) {
      this.username = username;
      this.password = password;
    }
  }

  // Start is called before the first frame update
  void Start() {
    
  }

  // Update is called once per frame
  void Update() {
    if (Input.GetKeyDown(KeyCode.Return)) {
      Authenticate();
    }
  }

  public void Authenticate() {
    StartCoroutine(RequestAuthenticate());
  }

  public void Register() {
    StartCoroutine(RequestRegister());
  }

  public IEnumerator RequestRegister() {
    string username = GameObject.Find("Register/Username").GetComponent<InputField>().text;
    string password = GameObject.Find("Register/Password").GetComponent<InputField>().text;
    string email = GameObject.Find("Register/Email").GetComponent<InputField>().text;

    var reg = new Registration(username, password, email);
    string json = JsonUtility.ToJson(reg);
    byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(json);

    var uwr = new UnityWebRequest(serverUrl + "/register", "POST");
    uwr.uploadHandler = (UploadHandler) new UploadHandlerRaw(jsonToSend);
    uwr.downloadHandler = (DownloadHandler) new DownloadHandlerBuffer();
    uwr.SetRequestHeader("Content-Type", "application/json");

    yield return uwr.SendWebRequest();

    Debug.Log("Status code: " + uwr.responseCode);

    if (uwr.isNetworkError) {
      Debug.Log("Error: " + uwr.error);
    } else {
      Debug.Log(uwr.downloadHandler.text);
      SwitchView();
    }
  }

  public IEnumerator RequestAuthenticate() {
    string username = GameObject.Find("Login/Username").GetComponent<InputField>().text;
    string password = GameObject.Find("Login/Password").GetComponent<InputField>().text;

    var auth = new Authentication(username, password);
    string json = JsonUtility.ToJson(auth);
    byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(json);

    var uwr = new UnityWebRequest(serverUrl + "/authenticate", "POST");
    uwr.uploadHandler = (UploadHandler) new UploadHandlerRaw(jsonToSend);
    uwr.downloadHandler = (DownloadHandler) new DownloadHandlerBuffer();
    uwr.SetRequestHeader("Content-Type", "application/json");

    yield return uwr.SendWebRequest();

    Debug.Log("Status code: " + uwr.responseCode);

    if (uwr.isNetworkError) {
      Debug.Log("Error: " + uwr.error);
    } else {
      Debug.Log(uwr.downloadHandler.text);
      PlayerController playerCtl = GameObject.Find("Player").GetComponent<PlayerController>();
      playerCtl.username = username;
      playerCtl.isConnected = true;
      playerCtl.StartUpdateValuesCoroutine();

      /* Load world scene */
      SceneManager.LoadScene("World");
    }
  }

  public void SwitchView() {
    foreach (Transform t in transform.parent) {
      t.gameObject.SetActive(!t.gameObject.activeSelf);
    }
  }
}
