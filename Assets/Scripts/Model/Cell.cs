[System.Serializable]
public class Cell {
  public int voronoiId;
  public Site site;
  public Halfedge[] halfedges;
  public Point[] pointsSorted;
  public string type;
  public int[] neighboors;
  public bool closeMe;
}