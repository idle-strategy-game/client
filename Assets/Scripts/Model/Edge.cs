[System.Serializable]
public class Edge {
  public Site lSite;
  public Site rSite;
  public Point va;
  public Point vb;
}