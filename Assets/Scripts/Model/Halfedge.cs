[System.Serializable]
public class Halfedge {
  public Site site;
  public Edge edge;
  public float angle;
}