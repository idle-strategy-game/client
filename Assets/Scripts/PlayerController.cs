﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerController : MonoBehaviour {
  [System.Serializable]
  public class Resources {
    public int wood;
    public int stone;
    public int gold;
    public int food;
  }
  public Resources resources = new Resources();
  private string serverUrl = "http://localhost:8081";
  public string username;
  public string email;
  public bool isConnected = false;
  
  // Start is called before the first frame update
  void Start() {
    
  }

  // Update is called once per frame
  void Update() {
      
  }

  void Awake() {
    GameObject[] objs = GameObject.FindGameObjectsWithTag("Player");

    if (objs.Length > 1) {
      Destroy(this.gameObject);
    }

    DontDestroyOnLoad(this.gameObject);
  }

  public void FetchResources() {
    StartCoroutine(FetchResourcesRequest());
  }

  IEnumerator FetchResourcesRequest() {
    using (UnityWebRequest uwr = UnityWebRequest.Get(serverUrl + "/resources?username=" + username)) {
      // Request and wait for the desired page.
      yield return uwr.SendWebRequest();

      if (uwr.isNetworkError) {
        Debug.Log("Error while fetching resources: " + uwr.error);
      } else {
        Debug.Log("Resources: " + uwr.downloadHandler.text);
        resources = JsonUtility.FromJson<Resources>(uwr.downloadHandler.text);
        GameObject.Find("UI").GetComponent<UiController>().UpdateResources();
      }
    }
  }

  IEnumerator UpdateValuesCoroutine() {
    while (true) {
      FetchResources();
      yield return new WaitForSeconds(60);
    }
  }

  public void StartUpdateValuesCoroutine() {
    StartCoroutine(UpdateValuesCoroutine());
  }
}
