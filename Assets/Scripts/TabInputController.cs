﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TabInputController : MonoBehaviour {
  public InputField nextField;
  
  // Start is called before the first frame update
  void Start() {
      
  }

  // Update is called once per frame
  void Update() {
    if (GetComponent<InputField>().isFocused && Input.GetKeyDown(KeyCode.Tab)) {
      nextField.ActivateInputField();
    }
  }
}
