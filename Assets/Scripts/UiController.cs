﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UiController : MonoBehaviour {
  public Text wood;
  public Text stone;
  public Text gold;
  public Text food;
  public GameObject selectedCell;

  // Start is called before the first frame update
  void Start() {
    
  }

  // Update is called once per frame
  void Update() {
    
  }

  public void UpdateResources() {
    PlayerController playerCtl = GameObject.Find("Player").GetComponent<PlayerController>();
    wood.text = playerCtl.resources.wood.ToString();
    stone.text = playerCtl.resources.stone.ToString();
    gold.text = playerCtl.resources.gold.ToString();
    food.text = playerCtl.resources.food.ToString();
  }

  
  public void Logout() {
    /* Load world scene */
    PlayerController playerCtl = GameObject.Find("Player").GetComponent<PlayerController>();
    playerCtl.username = null;
    playerCtl.isConnected = false;
    SceneManager.LoadScene("Login");
  }

  public void updateSelectedCell(GameObject cell) {
    GameObject uiCellPanel = GameObject.Find("UI").transform.Find("Cell").gameObject;
    if (this.selectedCell != null) {
      this.selectedCell.GetComponent<CellController>().toggleSelected(); // Deselect current cell
    }
    if (this.selectedCell == cell) {
      Debug.Log("Deselecting " + this.selectedCell.name);
      this.selectedCell = null; // Second click on same cell : deselect;
      uiCellPanel.SetActive(false);
    } else if (cell != null) {
      Debug.Log("Selecting " + cell.name);
      this.selectedCell = cell;
      this.selectedCell.GetComponent<CellController>().toggleSelected();
      uiCellPanel.SetActive(true);
    }
  }
}
